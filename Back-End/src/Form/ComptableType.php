<?php

namespace App\Form;

use App\Entity\Comptable;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComptableType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('date', DateTimeType::class, [
                'widget' => 'single_text',
                'date_label' => 'date'])
            ->add('reference')
            ->add('libelle')
            ->add('montant')
            ->add('montantEntree')
            ->add('montantSortie')
            ->add('observation')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comptable::class,
            'csrf_protection' => false,
            "allow_extra_fields" => true
        ]);
    }
}
