<?php

namespace App\Entity;

use App\Repository\ComptableRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=ComptableRepository::class)
 */
class Comptable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("list-comptable")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     * @Groups("list-comptable")
     */
    private $numero;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("list-comptable")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("list-comptable")
     */
    private $reference;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("list-comptable")
     */
    private $libelle;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("list-comptable")
     */
    private $montant;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("list-comptable")
     */
    private $montantEntree;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups("list-comptable")
     */
    private $montantSortie;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("list-comptable")
     */
    private $observation;

    /**
     * @ORM\ManyToOne(targetEntity=Utilisateur::class, inversedBy="userComptables")
     * @Groups("list-comptable")
     */
    private $idUser;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getMontant(): ?string
    {
        return $this->montant;
    }

    public function setMontant(string $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getMontantEntree(): ?float
    {
        return $this->montantEntree;
    }

    public function setMontantEntree(float $montantEntree): self
    {
        $this->montantEntree = $montantEntree;

        return $this;
    }

    public function getMontantSortie(): ?float
    {
        return $this->montantSortie;
    }

    public function setMontantSortie(?float $montantSortie): self
    {
        $this->montantSortie = $montantSortie;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->observation;
    }

    public function setObservation(string $observation): self
    {
        $this->observation = $observation;

        return $this;
    }

    public function getIdUser(): ?Utilisateur
    {
        return $this->idUser;
    }

    public function setIdUser(?Utilisateur $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }
}
