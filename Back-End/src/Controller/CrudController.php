<?php

namespace App\Controller;

use App\Entity\Comptable;
use App\Form\ComptableType;
use App\Repository\ComptableRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/api")
 */
class CrudController extends AbstractController
{

    const STATUS = "status";
    const TYPE = "type";
    const MESSAGE = "message";
    const CONTENT_TYPE = 'Content-Type';
    const APPLICATION_TYPE = 'application/json';
    const GROUPS = 'groups';
    const NOTFOUND="Ressource non trouvée !!";


    /**
     * @Route("/element", name="element_comptable", methods={"POST","GET"})
     */
    public function addElement(Request $request, EntityManagerInterface $entityManager, SerializerInterface $serializer, ValidatorInterface $validator)
    {
        $values=json_decode($request->getContent(),true);
        if(!$values){
            $values=$request->request->all();
        }
        $comptable = new Comptable();
        $form = $this->createForm(ComptableType::class, $comptable);
        $form->submit($values);
        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->json($validator->validate($form));
        }
        $comptable->setIdUser($this->getUser());
        $errors = $validator->validate($form);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json',
                'message' => 'Error de remplissage'
            ]);
        }
        $entityManager->persist($comptable);
        $entityManager->flush();
        return $this->json([
            'status' => 201,
            'message' => "Votre requete a été validée"
        ]);
    }

    /**
     * @Route("/element/{id}", name="modifier_element", methods={"POST","GET"})
     */
    public function EditerComptable(ComptableRepository $repo,Request $request, SerializerInterface $serializer, ValidatorInterface $validator, EntityManagerInterface $entityManagerInterface,Comptable $comptable = null, $id = null)
    {
        $comptable=$repo->findOneBy(["id" => $id]);
        if ($id && !$comptable) {
            throw new HttpException(404, 'Ce Élement comptable n\'existe pas!');
        }
        $data = json_decode($request->getContent());
        if(!$data){
            $data=$request->request->all();
        }
        foreach ($data as $key => $value){
            if($key && !empty($value)) {
                $name = ucfirst($key);
                $setter = 'set'.$name;
                $comptable->$setter($value);
            }
        }
        $errors = $validator->validate($comptable);
        if(count($errors)) {
            $errors = $serializer->serialize($errors, 'json');
            return new Response($errors, 500, [
                'Content-Type' => 'application/json'
            ]);
        }
        $entityManagerInterface->flush();
        $data=[
            'status' => 200,
            'message' => "L'élémént comptable a été mis à jour"
        ];
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/show/element", name="lister_element", methods={"POST","GET"})
     * @Route("/show/element/{id}", name="lister_un_element", methods={"GET","POST"})
     */
    public function listerElement(ComptableRepository $comptableRepository, SerializerInterface $serializer, Comptable $comptable = null, $id = null)
    {
        $comptable=$comptableRepository->findOneBy(["id" => $id]);
        if ($id && !$comptable) {
            throw new HttpException(404, 'Ce élement Comptable n\'existe pas!');
        }
        if (!$comptable) {
            $comptable = $comptableRepository->findAll();
        }
        $data = $serializer->serialize($comptable, 'json', [self::GROUPS => ['list-comptable']]);
        return new Response($data, 200, [self::CONTENT_TYPE => self::APPLICATION_TYPE]);
    }


    /**
     * @Route("/show/periode/element", name="lister_element_periode", methods={"POST","GET"})
     */
    public function listerElementPeriode(Request $request, ComptableRepository $comptableRepository, SerializerInterface $serializer)
    {
        $data = json_decode($request->getContent());
        if(!$data){
            $data=$request->request->all();
        }
        $comptable = $comptableRepository->findPeriode($data["dateDebut"],$data["dateFin"]);
        $data = $serializer->serialize($comptable, 'json', [self::GROUPS => ['list-comptable']]);
        return new Response($data, 200, [self::CONTENT_TYPE => self::APPLICATION_TYPE]);
    }

    /**
    * @Route("/element/{id}", name="delete_element_comptable", methods={"DELETE"})
    */
    public function delete(Comptable $comptable, EntityManagerInterface $entityManager)
    {
        $entityManager->remove($comptable);
        $entityManager->flush();
        return new Response(null, 204);
    }
}
