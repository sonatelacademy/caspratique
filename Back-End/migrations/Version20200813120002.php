<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200813120002 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comptable ADD id_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comptable ADD CONSTRAINT FK_5B64436779F37AE5 FOREIGN KEY (id_user_id) REFERENCES utilisateur (id)');
        $this->addSql('CREATE INDEX IDX_5B64436779F37AE5 ON comptable (id_user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comptable DROP FOREIGN KEY FK_5B64436779F37AE5');
        $this->addSql('DROP INDEX IDX_5B64436779F37AE5 ON comptable');
        $this->addSql('ALTER TABLE comptable DROP id_user_id');
    }
}
